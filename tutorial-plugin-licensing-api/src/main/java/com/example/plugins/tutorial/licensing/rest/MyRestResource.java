package com.example.plugins.tutorial.licensing.rest;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.example.plugins.tutorial.licensing.LicenseChecker;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * A resource of message.
 */
@Path("/message")
public class MyRestResource {

    private final LicenseChecker licenseChecker;

    public MyRestResource(LicenseChecker licenseChecker) {
        this.licenseChecker = licenseChecker;        
    }

    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getMessage()
    {
       if (!licenseChecker.validateLicense()) {
          return Response.status(Response.Status.FORBIDDEN).build();
       }

       return Response.ok(new MyRestResourceModel("Hello World")).build();
    }
}
package com.example.plugins.tutorial.licensing.servlet;

import com.example.plugins.tutorial.licensing.LicenseChecker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class MyServlet extends HttpServlet {
    private static final Logger log = LoggerFactory.getLogger(MyServlet.class);

    private final LicenseChecker licenseChecker;

    public MyServlet(LicenseChecker licenseChecker) {
        this.licenseChecker = licenseChecker;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        if (!licenseChecker.validateLicense()) {
            resp.sendError(HttpServletResponse.SC_FORBIDDEN, "Cannot proceed. Your license is invalid.");
        }

        resp.setContentType("text/html");
        resp.getWriter().write("<html><body>Hello World</body></html>");
    }
}
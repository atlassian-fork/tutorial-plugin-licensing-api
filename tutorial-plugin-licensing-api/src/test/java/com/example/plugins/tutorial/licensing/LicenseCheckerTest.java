package com.example.plugins.tutorial.licensing;

import com.atlassian.plugin.PluginController;
import com.atlassian.upm.api.license.*;
import com.atlassian.upm.api.license.entity.*;
import com.atlassian.upm.api.license.event.*;
import com.atlassian.upm.api.util.Option;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class LicenseCheckerTest {
    final String PLUGIN_KEY = "com.example.plugins.tutorial.tutorial-plugin-licensing-api";

    PluginLicense pluginLicense;
    PluginLicenseChangeEvent changeEvent;
    PluginLicenseCheckEvent checkEvent;

    PluginController pluginController;
    PluginLicenseManager pluginLicenseManager;
    PluginLicenseEventRegistry pluginLicenseEventRegistry;

    LicenseChecker licenseChecker;

    @Before
    public void setup() throws IOException {
        pluginLicense = mock(PluginLicense.class);
        changeEvent = new PluginLicenseAddedEvent(pluginLicense);
        checkEvent = new PluginLicenseCheckEvent(PLUGIN_KEY, Option.some(pluginLicense));

        pluginController = mock(PluginController.class);
        pluginLicenseManager = mock(PluginLicenseManager.class);
        pluginLicenseEventRegistry = mock(PluginLicenseEventRegistry.class);

        when(pluginLicense.isValid()).thenReturn(true);
        when(pluginLicense.getError()).thenReturn(Option.none(LicenseError.class));
        when(pluginLicenseManager.getLicense()).thenReturn(Option.some(pluginLicense));
        when(pluginLicenseManager.getPluginKey()).thenReturn(PLUGIN_KEY);

        licenseChecker = new LicenseChecker(pluginController, pluginLicenseManager, pluginLicenseEventRegistry);
    }

    @Test
    public void testHandleChangeEventForValidLicense() {
        licenseChecker.handleEvent(changeEvent);
        //verify 0 interactions
        Mockito.verify(pluginController, Mockito.never()).disablePlugin(PLUGIN_KEY);
    }

    @Test
    public void testHandleChangeEventForInvalidLicense() {
        when(pluginLicense.isValid()).thenReturn(false);
        licenseChecker.handleEvent(changeEvent);
        //verify 1 interaction
        Mockito.verify(pluginController).disablePlugin(PLUGIN_KEY);
    }

    @Test
    public void testHandleCheckEventForValidLicense()
    {
        licenseChecker.handleEvent(checkEvent);
        //verify 0 interactions
        Mockito.verify(pluginController, Mockito.never()).disablePlugin(PLUGIN_KEY);
    }

    @Test
    public void testHandleCheckEventForInvalidLicense()
    {
        when(pluginLicense.isValid()).thenReturn(false);
        licenseChecker.handleEvent(checkEvent);
        //verify 1 interaction
        Mockito.verify(pluginController).disablePlugin(PLUGIN_KEY);
    }

    @Test
    public void testValidateLicenseForValidLicense()
    {
        assertTrue(licenseChecker.validateLicense());
    }

    @Test
    public void testValidateLicenseForInvalidLicense()
    {
        when(pluginLicense.isValid()).thenReturn(false);
        assertFalse(licenseChecker.validateLicense());
    }
}
